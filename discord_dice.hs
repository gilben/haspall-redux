{-# LANGUAGE OverloadedStrings #-}
module Discord.Dice where

import qualified Data.Text as T -- Shortened for readability
import System.Random (getStdRandom, randomR)
import System.IO.Unsafe (unsafePerformIO)
import Discord
import Discord.Aid
import WordOps

data Dice = Dice {
    diceCount::Int,
    diceSize::Int
} deriving Show

isDiceCmd :: T.Text -> Bool
isDiceCmd  = isCmdReg "^(!roll)"

diceCmds :: Message -> (RestChan, y, z) -> IO()
diceCmds m dis
  | isCmdReg "^!roll " content = reply $ calcDice contentTail
      where reply           = discordReply m dis
            replySplit      = discordReplySplit m dis
            replyEmbed      = discordReplyEmbed m dis
            content         = messageText m -- Message sent in T.Text form
            contentTail     = wordTail content -- Content missing the first word (usually the command)
            argInt          = nthWordToInt 1 content

-- Lists Dice commands
diceCmdList :: T.Text
diceCmdList  = "`roll`"

-- Returns help information for Dice commands
diceCmdHelp :: T.Text -> T.Text
diceCmdHelp "roll" = "Usage: `!roll #d#+#d#...`\nRolls dice separated be +.\nExample: `!roll 2d6+4d4`"
diceCmdHelp other  = ""

-- Calculates a dice roll output for a given text input
calcDice   :: T.Text -> T.Text
calcDice text = T.intercalate " | " $ map (packDice . rollDice) $ numToDice $ diceNums text

-- Packs the output of one dice into text
packDice        :: [IO Int] -> T.Text
packDice results = T.intercalate ", " $ map (T.pack . show . unsafePerformIO) results

-- Performs the random roll for a given dice to a list of outputs
rollDice     :: Dice -> [IO Int]
rollDice dice = replicate (diceCount dice) ((\x -> getStdRandom (randomR (1,x))) (diceSize dice))

-- Parse dice out of a given text to 
numToDice     :: [Maybe Int] -> [Dice]
numToDice []   = []
numToDice [x]  = []
numToDice (x1:(x2:xs)) = case (x1,x2) of
                        (Just v1, Just v2) -> Dice v1 v2 : numToDice xs
                        (Just v1, Nothing) -> numToDice xs
                        (Nothing, Just v2) -> numToDice xs

diceNums     :: T.Text -> [Maybe Int] 
diceNums text = wordsInts
              $ T.intercalate " "
              $ concatMap (T.splitOn "+") (T.splitOn "d" text)