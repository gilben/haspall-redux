{-# LANGUAGE OverloadedStrings #-}

module Discord.Aid where

import Data.Char (toLower)
import qualified Data.Text as T
import Text.Regex.Posix ((=~))
import System.IO.Unsafe (unsafePerformIO)
import Data.Time.Clock (getCurrentTime)
import Discord
import WordOps

-- Takes a message and performs the restcall output the the channel
discordReply          :: Message -> (RestChan, y, z) -> T.Text -> IO ()
discordReply m dis out = do
    resp <- restCall dis $ CreateMessage (messageChannel m) out
    print resp
    putStrLn ""

-- Takes a message and outputs in chunks of 2000 characters per discord's message limit
discordReplySplit          :: Message -> (RestChan, y, z) -> T.Text -> IO ()
discordReplySplit m dis out = do -- Possibly split if output is > 2000, more intensive than reply
    resp <- restCall dis $ CreateMessage (messageChannel m) $ head $ discordSplit out 
    print resp
    putStrLn ""
    if tail (discordSplit out) /= [] then
        discordReplySplit m dis $ T.concat (tail $ discordSplit out)
    else
        putStrLn ""

-- Takes an embed object and outputs that to the channel
discordReplyEmbed                       :: Message -> (RestChan, y, z) -> T.Text -> Embed -> IO ()
discordReplyEmbed m dis outText outEmbed = do
    print outEmbed
    resp <- restCall dis $ CreateMessageEmbed (messageChannel m) outText outEmbed
    print resp
    putStrLn ""


-- We don't want to respond to other discord bots, so test to see if a user is a bot using the
-- library function userIsBot.
-- If it's a webhook, just treat it as if it's a bot
fromBot :: Message -> Bool
fromBot m = case messageAuthor m of
              Right u         -> userIsBot u
              Left _webhookid -> True

-- Returns the username as text, unless it's a webhookid
userText  :: Message -> T.Text
userText m = case messageAuthor m of
                 Right u -> T.pack $ userName u
                 Left _webhookid -> T.pack ""
 
-- Splits text into chunks smaller than 2000 characters
discordSplit :: T.Text -> [T.Text]
discordSplit  = T.chunksOf 2000

-- Trims a block of text to below 2000 with a ... at the end
discordTrim     :: T.Text -> T.Text
discordTrim text
    | T.length text > 2000 = T.take 1997 text +++ "..."
    | otherwise            = text

-- Matches a prefix with some text
-- Used to detect command pings
isCmd       :: T.Text -> T.Text -> Bool
isCmd prefix = T.isPrefixOf prefix . T.map toLower

-- Matches a regex with some text
-- Used to detect more complex command pings
isCmdReg              :: T.Text -> T.Text -> Bool
isCmdReg match query = T.unpack query =~ T.unpack match

-- Takes in a title, description and color, then returns an Embed object for those items
embedPack                     :: T.Text -> T.Text -> T.Text -> T.Text -> Embed
embedPack title desc url color = case color of 
                                "grey" -> packUp 0x333333
                                _      -> packUp 0x42f4d1
        where packUp hex = Embed (T.unpack title) "" (T.unpack desc) (T.unpack url) (unsafePerformIO getCurrentTime) hex []