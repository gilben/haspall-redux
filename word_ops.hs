{-# LANGUAGE OverloadedStrings #-}

module WordOps where

import qualified Data.Text as T
import qualified Text.Read  as TR (readMaybe)
import Data.Maybe (fromMaybe)

-- A simple overload of +++ to add two T.Text objects together as if they were String
-- Simply because doing `T.append` over and over again is ugly :)
(+++)    :: T.Text -> T.Text -> T.Text
(+++) l r = l `T.append` r

-- Returns the text of the words following the first word, but not including the first word
-- Ex: wordTail "foo bar blat" = "bar blat"
wordTail     :: T.Text -> T.Text
wordTail text =  T.intercalate " " . tail $ T.words text

-- Returns the text of the words following the first n words, not including the first n words
wordDrop       :: Int -> T.Text -> T.Text
wordDrop n text = T.intercalate " " $ drop n $ T.words text

-- Returns a Maybe Int for every word of the given T.Text
nthWordMaybeInt     :: Int -> [Maybe Int] -> Int
nthWordMaybeInt n ns = fromMaybe 0 (ns !! n)

-- Assumes the nthWord converts cleanly using read, grabs the nth word from a T.Text as an Int
nthWordToInt       :: Int -> T.Text -> Int
nthWordToInt n text = read (T.unpack $ T.words text !! n)::Int

-- Returns a Maybe Int for every word in a T.Text
wordsInts     :: T.Text -> [Maybe Int]
wordsInts text = map ((\x -> TR.readMaybe x :: Maybe Int) . T.unpack) 
               $ T.words text

-- Takes a command and wraps it in ``` ``` for pretty formatting
codeblock     :: T.Text -> T.Text
codeblock text = "```\n" +++ text +++ "```"

-- Takes a command and wraps it in ```haskell ``` for pretty haskell syntax formatting
haskellBlock     :: T.Text -> T.Text
haskellBlock text = "```haskell\n" +++ text +++ "```"


-- Adds line numbers from a list of integers and a list of Text and adds them together
lineNumber      :: [Int] -> [T.Text] -> [T.Text]
lineNumber [] [] = []
lineNumber (x:xs) [] = (T.pack . show) x : lineNumber xs []
lineNumber [] (y:ys) = y : lineNumber [] ys
lineNumber (x:xs) (y:ys) = (T.pack . show) x +++ ". " +++ y : lineNumber xs ys