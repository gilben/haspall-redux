{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Exception (finally)
import Data.Monoid ((<>))
import qualified Data.Text as T -- Shortened for readability
import qualified Data.Text.IO as TIO (readFile)

import Discord
import Discord.Aid
import Discord.Hoogle
import Discord.Dice

import WordOps

-- Kick off the bot
main :: IO ()
main  = do
  token <- T.strip <$> TIO.readFile "./auth-token.secret" -- Grab the token string, not included for secrecy
  dis   <- loginRestGateway (Auth token) -- Authenticate with token to Discord's servers
  finally (botLoop dis) -- Run the bot
          (stopDiscord dis) -- After this finishes, close the bot properly
 
-- Main botloop
botLoop    :: (RestChan, Gateway, z) -> IO ()
botLoop dis = do
  e <- nextEvent dis
  case e of
      Left er                 -> putStrLn ("Event error: " <> show er)
      Right (MessageCreate m) -> do
        processMessage m dis
        botLoop dis
      _                       -> botLoop dis

processMessage      :: Message -> (RestChan, y, z) -> IO ()
processMessage m dis 
  | fromBot m                          = return () -- Ignore the bot, do nothing
  | isCmd "!echo " content             = reply $ wordTail content
  | isCmd "!help " content             = reply $ showHelp $ argString 1
  | isCmd "!help" content              = reply $ showHelp "help"
  | isHoogleCmd content                = hoogleCmds m dis
  | isDiceCmd content                  = diceCmds m dis
  | isCmdReg "^!list$" content         = replyEmbed "" $ embedPack "Available Commands:" commandList "" ""
  | isCmd "!ping" content              = reply "Pong"
  | otherwise                          = return () -- It's not a command, also do nothing
      where reply           = discordReply m dis
            replySplit      = discordReplySplit m dis
            replyEmbed      = discordReplyEmbed m dis
            content         = messageText m -- Message sent in T.Text form
            argInt          = nthWordToInt 1 content
            argString n     = T.words content !! n


-- Help text for all commands a user can enter
showHelp        :: T.Text -> T.Text
showHelp "echo"  = "Usage: `!echo <string>`\nRepeats `string` that is given"
showHelp "help"  = "Usage: `!help <command>`\nDisplays help information for a specific `command`"
showHelp "list"  = "Usage: `!list`\nReturns list of available commands"
showHelp "ping"  = "Usage: `!ping`\nReturns pong"
showHelp other
  | hoogleCmdHelp other /= "" = hoogleCmdHelp other
  | diceCmdHelp other /= "" = diceCmdHelp other
  | otherwise = "Command " +++ other +++ " is not valid."
--showHelp notFound = "Command " +++ notFound +++ " is not valid."



-- List of available command.s
commandList :: T.Text
commandList = "`echo` `help` " +++ hoogleCmdList +++ " `list` `ping` " +++ diceCmdList +++ "\n\n\
              \Use `!help <command>` to see details for usage.\n\n\
              \All commands prefixed with `!`"