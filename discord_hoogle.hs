{-# LANGUAGE OverloadedStrings #-}

module Discord.Hoogle where

import qualified Data.Text as T -- Shortened for readability
import System.IO.Unsafe (unsafePerformIO)
import Hoogle
import Discord
import Discord.Aid
import WordOps

-- Detects a hoogle command
isHoogleCmd :: T.Text -> Bool
isHoogleCmd = isCmdReg "^((!hg )|(!hg [0-9]+ )|(!hgnum [0-9]+ )|(!hglist [0-9]+ ))" 

-- Executes hoogle based command
hoogleCmds       :: Message -> (RestChan, y, z) -> IO ()
hoogleCmds m dis
  | isCmdReg "^!hg [0-9]+ " content     = replyEmbed "" $ embedPack (hoogleHeader $ runInfoHoogle argInt) (discordTrim $ hoogleContent $ runInfoHoogle argInt) (urlHoogle argInt contentTail) ""
  | isCmd "!hg " content                = replyEmbed "" $ embedPack (hoogleHeader $ runInfoHoogle 1) (discordTrim $ hoogleContent $ runInfoHoogle 1) (urlHoogle 1 contentTail) ""
  | isCmdReg "^!hgnum [0-9]+ " content  = replySplit $ topNHoogle argInt (wordDrop 2 content) True
  | isCmdReg "^!hglist [0-9]+ " content = replySplit $ topNHoogle argInt (wordDrop 2 content) True
      where replySplit      = discordReplySplit m dis
            replyEmbed      = discordReplyEmbed m dis
            content         = messageText m -- Message sent in T.Text form
            contentTail     = wordTail content -- Content missing the first word (usually the command)
            argInt          = nthWordToInt 1 content
            runInfoHoogle n = infoHoogle n contentTail

-- Lists hoogle commands
hoogleCmdList :: T.Text
hoogleCmdList  = "`hg` `hglist` `hgnum`"

-- Returns usage information for Hoogle commands
hoogleCmdHelp :: T.Text -> T.Text
hoogleCmdHelp "hg"     = "Usage: `!hg [entry number] <query>`\nReturns the Hoogle information block for `query`, indexed by `entry number`\nIf `entry number` is omitted, defaults to the first result"
hoogleCmdHelp "hgnum"  = "Usage: `!hgnum [number of entries] <query>`\nReturns `number of entries` results for `query`, with links to the Hoogle web source"
hoogleCmdHelp "hglist" = "Usage: `!hglist [number of entries] <query>`\nReturns `number of entries` results for `query`, with links to the Hoogle web source"
hoogleCmdHelp other    = ""

-- Gets the URL for the nth hoogle entry for the given query
urlHoogle           :: Int -> T.Text -> T.Text
urlHoogle n query = case takeHoogle n query of
                            [] -> ""
                            xs -> T.pack . targetURL $ last xs


-- Gets n hoogle results as T.Text separated by newlines
topNHoogle                 :: Int -> T.Text -> Bool -> T.Text
topNHoogle n query showLink = T.intercalate "\n" $ 
                              (\x -> lineNumber [1..length x] x) $
                              map (T.pack . targetResultDisplay showLink) $
                              takeHoogle n query

-- Gets the nth information block for the given query
infoHoogle        :: Int -> T.Text -> T.Text
infoHoogle n query = case takeHoogle n query of 
                          [] -> T.pack "No results found"
                          xs -> T.pack $ targetInfo $ last xs

-- Returns n hoogle results as a list of Targets
-- Technically unsafe, but we need a regular T.Text at the end of the chain
-- to pass to the CreateMessage part of the API
takeHoogle        :: Int -> T.Text -> [Target]
takeHoogle n query = unsafePerformIO $ withDatabase(unsafePerformIO defaultDatabaseLocation) $ \db ->
  case searchDatabase db $ show query of
      [] -> return [] 
      xs -> return . take n $ xs

-- Grab the first two lines of a block of text
hoogleHeader      :: T.Text -> T.Text
hoogleHeader text   = T.intercalate "\n" $ take 2 $ T.splitOn "\n" text

-- Grab all but the first two lines of a block of text
hoogleContent     :: T.Text -> T.Text
hoogleContent text = T.intercalate "\n" $ drop 2 $ T.splitOn "\n" text