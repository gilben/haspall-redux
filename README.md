# Haspall

A multifunctional discord bot written in Haskell

# Cabal Dependencies
regex-posix
hoogle
discord-haskell

# Usage:

Create a Discord application as you would for any bot (TODO details / link for that here)
Copy the bot token into a new file called `auth-token.secret`.

Install `regex-posix`, `hoogle`, and `discord-haskell` using `cabal install`. All other necessary packages should be in the base Haskell prelude.

Compile the bot with `make`.

Run using `./haspall`.

To clean the directory of object files use `make clean`.